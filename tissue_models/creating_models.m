%% Extraction of context-specific models

%#########################################################################%
%                                                                         %
%                         WITH THE SALMON MODEL                           %
%                                                                         %
%#########################################################################%


%% Initialise The COBRA Toolbox
initCobraToolbox(false) %don't update the toolbox


%% Change COBRA solver to use gurobi
changeCobraSolver ('gurobi', 'all'); 

%% Read model
model = readCbModel(['matlab_models' filesep 'sasa.mat']);
load(['data' filesep 'sample_names.mat']);

% Change lower bound for Biomass reaction to 1, 
% to demand it to carry flux in the creation of the context-specific models
biomass_idx = find(strcmp(model.rxns, 'Biomass'));
model.lb(biomass_idx) = 1;


%% Load and prepare parameters

load(['data' filesep 'expressionRxns_sets']);
load(['data/FASTCORE' filesep 'core_sets']);
load(['data/INIT' filesep 'weights_list']);
load(['data/MBA' filesep 'high_sets_default']);
load(['data/MBA' filesep 'medium_sets_default']);
load(['data/MBA' filesep 'high_sets_strict']);
load(['data/MBA' filesep 'medium_sets_strict']);
load(['data/mCADRE' filesep 'confidence_sets']);
load(['data/mCADRE' filesep 'ubiquity_sets']);

% Transpose parameters as needed
expressionRxns_list_t = expressionRxns_list.';
weights_list_t = weights_list.';
confidence_sets_t = confidence_sets.';
ubiquity_sets_t = ubiquity_sets.';


%% CREATING MODELS IN DOUBLE FOR-LOOP (FOR WINDOWS)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                               WINDOWS                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%MEMs = {['GIMME'], ['fastCore'], ['iMAT'], ['MBA_strict'], ['MBA_default'], ['mCADRE'], ['INIT']};
MEMs = {['INIT']};
threshold = 5*log(2);

% Iterate over Model Extraction Methods (MEMs)
for ix = 1:length(MEMs)
    mem = MEMs{ix};
    
    % Create models with GIMME
    if ismember(mem, 'GIMME')
        options.solver = mem;
        options.threshold = threshold;
        
        for i=1:length(sample_names)
            options.expressionRxns = expressionRxns_list_t(:, i);
            filename = strcat('C:/Users/havardmo/', ...
                              'OneDrive - Norwegian University of Life Sciences/', ...
                              'matlab/GIMME/', ...
                              strtrim(sample_names(i, :)), '.mat');
            GIMME_model = createTissueSpecificModel(model, options);
            save(filename,'GIMME_model');
        end
    end
    
    % Create models with FASTCORE
    if ismember(mem, 'fastCore')
        options.solver = mem;
        
        for i=1:length(sample_names)
            options.core = core_sets{i};
            filename = strcat('C:/Users/havardmo/', ...
                              'OneDrive - Norwegian University of Life Sciences/', ...
                              'matlab/FASTCORE/', ...
                              strtrim(sample_names(i, :)), '.mat'); 
            FASTCORE_model = createTissueSpecificModel(model, options);
            save(filename,'FASTCORE_model');
        end
    end
    
    % Create models with iMAT
    if ismember(mem, 'iMAT')
        options.solver = mem;
        options.threshold_ub = threshold;
        options.threshold_lb = threshold;
        
        for i=1:length(sample_names)
            options.expressionRxns = expressionRxns_list_t(:, i);
            filename = strcat('C:/Users/havardmo/', ...
                              'OneDrive - Norwegian University of Life Sciences/', ...
                              'matlab/iMAT/', ...
                              strtrim(sample_names(i, :)), '.mat');
            iMAT_model = createTissueSpecificModel(model, options);
            save(filename,'iMAT_model');
        end
    end
    
    % Create models with MBA
    if ismember(mem, 'MBA_default')
        options.solver = 'MBA';
        options.tol = 1e-06;

        for i=1:length(sample_names)
            options.high_set = high_sets_default{i};
            options.medium_set = medium_sets_default{i};
            filename = strcat('C:/Users/havardmo/', ...
                              'OneDrive - Norwegian University of Life Sciences/', ...
                              'matlab/MBA/default_thresh/', ...
                              strtrim(sample_names(i, :)), '.mat');           
            MBA_model = createTissueSpecificModel(model, options);
            save(filename,'MBA_model');
        end
    end
    
    % Create models with MBA
    if ismember(mem, 'MBA_strict')
        options.solver = 'MBA';
        options.tol = 1e-06;

        for i=1:length(sample_names)
            options.high_set = high_sets_strict{i};
            options.medium_set = medium_sets_strict{i};
            filename = strcat('C:/Users/havardmo/', ...
                              'OneDrive - Norwegian University of Life Sciences/', ...
                              'matlab/MBA/strict_thresh/', ...
                              strtrim(sample_names(i, :)), '.mat');           
            MBA_model = createTissueSpecificModel(model, options);
            save(filename,'MBA_model');
        end
    end
    
    % Create models with mCADRE
    if ismember(mem, 'mCADRE')
        options.solver = mem;
        options.tol = 1e-06;

        for i=1:length(sample_names)
            options.ubiquityScore = ubiquity_sets_t(:, i);
            options.confidenceScores = confidence_sets_t(:, i);
            filename = strcat('C:/Users/havardmo/', ...
                              'OneDrive - Norwegian University of Life Sciences/', ...
                              'matlab/mCADRE/', ...
                              strtrim(sample_names(i, :)), '.mat');          
            mCADRE_model = createTissueSpecificModel(model, options);
            save(filename,'mCADRE_model');
        end
    end
    
    % Create models with INIT
    if ismember(mem, 'INIT')
        options.solver = mem;

        for i=1:length(sample_names)
            options.weights = weights_list_t(:, i);
            filename = strcat('C:/Users/havardmo/', ...
                              'OneDrive - Norwegian University of Life Sciences/', ...
                              'matlab/INIT/', ...
                              strtrim(sample_names(i, :)), '.mat');
            INIT_model = createTissueSpecificModel(model, options);
            save(filename,'INIT_model');
        end
    end
    
    clear options   % Reset options variable (remove from workspace)
end














%% CREATING MODELS IN DOUBLE FOR-LOOP (MAC-TESTED)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                MAC                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



MEMs = {['GIMME'], ['fastCore'], ['iMAT'], ['MBA'], ['mCADRE'], ['INIT']};
threshold = 5*log(2);

% Iterate over Model Extraction Methods (MEMs)
for ix = 1:length(MEMs)
    mem = MEMs{ix};
    
    % Create models with GIMME
    if ismember(mem, 'GIMME')
        options.solver = mem;
        options.threshold = threshold;
        
        for i=1%:length(sample_names)
            options.expressionRxns = expressionRxns_list_t(:, i);
            filename = strcat('matlab_models/test/GIMME/', ... 
                              strtrim(sample_names(i, :)), '.mat');
            GIMME_model = createTissueSpecificModel(model, options);
            save(filename,'GIMME_model');
        end
    end
    
    % Create models with FASTCORE
    if ismember(mem, 'fastCore')
        options.solver = mem;
        for i=1%:length(sample_names)
            options.core = core_sets{i};
            filename = strcat('matlab_models/test/FASTCORE/', ... 
                              strtrim(sample_names(i, :)), '.mat'); 
            FASTCORE_model = createTissueSpecificModel(model, options);
            save(filename,'FASTCORE_model');
        end
    end
    
    % Create models with iMAT
    if ismember(mem, 'iMAT')
        options.solver = mem;
        options.threshold_ub = threshold;
        options.threshold_lb = threshold;
        
        for i=1%:length(sample_names)
            options.expressionRxns = expressionRxns_list_t(:, i);
            filename = strcat('matlab_models/test/iMAT/', ... 
                              strtrim(sample_names(i, :)), '.mat');
            iMAT_model = createTissueSpecificModel(model, options);
            save(filename,'iMAT_model');
        end
    end
    
    % Create models with MBA
    if ismember(mem, 'MBA')
        options.solver = mem;

        for i=1%:length(sample_names)
            options.high_set = high_sets{i};
            options.medium_set = medium_sets{i};
            filename = strcat('matlab_models/test/MBA/', ...
                              strtrim(sample_names(i, :)), '.mat');           
            MBA_model = createTissueSpecificModel(model, options);
            save(filename,'MBA_model');
        end
    end
    
    % Create models with mCADRE
    if ismember(mem, 'mCADRE')
        options.solver = mem;

        for i=1%:length(sample_names)
            options.ubiquityScore = ubiquity_sets_t(:, i);
            options.confidenceScores = confidence_sets_t(:, i);
            filename = strcat('matlab_models/test/mCADRE/', ... 
                              strtrim(sample_names(i, :)), '.mat');          
            mCADRE_model = createTissueSpecificModel(model, options);
            save(filename,'mCADRE_model');
        end
    end
    
    % Create models with INIT
    if ismember(mem, 'INIT')
        options.solver = mem;
        %options.runtime = 30;

        for i=1%:length(sample_names)
            options.weights = weights_list_t(:, i);
            filename = strcat('matlab_models/test/INIT/', ... 
                              strtrim(sample_names(i, :)), '.mat');
            INIT_model = createTissueSpecificModel(model, options);
            save(filename,'INIT_model');
        end
    end
    
    clear options   % Reset options variable (remove from workspace)
end








%% INDIVIDUAL CREATION AND DEBUGGING (EARLY WORK)



%% MEM: iMAT

load(['data' filesep 'expressionRxns_sets.mat']);
expressionRxns_list_t = expressionRxns_list.';  % Transpose the variable

options.solver = 'iMAT';
options.threshold_ub = 5*log(2);
options.threshold_lb = 5*log(2);


%% DEBUGGING
%options.core = model.rxns([391, 713:718]); %391: ATP ﻿synthase, the rest is biomass-related reactions
options.expressionRxns = expressionRxns_list_t(:, 1);
iMAT_model = createTissueSpecificModel(model, options);
%filename = strcat('data/test/models/', strtrim(sample_names(1, :)), '_iMAT.mat');
filename = strcat('matlab_models/test/iMAT', strtrim(sample_names(1, :)), '_iMAT.mat');
save(filename, 'iMAT_model');

%% Creating and saving the models, one for every sample in the dataset.

for i=1:length(sample_names)
    options.expressionRxns = expressionRxns_list_t(:, i);
    filename = strcat('matlab_models/iMAT/', ... 
                      strtrim(sample_names(i, :)), '.mat');
    %iMAT_model = createTissueSpecificModel(model, options);
    %save(filename,'iMAT_model');
end


%% MEM: INIT

load(['data/INIT' filesep 'weights_list']);
weights_list_t = weights_list.';  % Transpose the variable
%weights_list_t(713:718, :) = weights_list_t(718, 1);

%% DEBUGGING

options.solver = 'INIT';
%options.runtime = 500;
options.weights = weights_list_t(:, 1);
INIT_model = createTissueSpecificModel(model, options);
%filename = strcat('data/test/models/', strtrim(sample_names(1, :)), '_INIT.mat');
filename = strcat('data/test/>0/models/', strtrim(sample_names(1, :)), '_INIT.mat');
save(filename, 'INIT_model');



%% Creating and saving the models, one for every sample in the dataset.

options.solver = 'INIT';
%options.runtime = 500;

for i=1:length(sample_names)
    options.weights = weights_list_t(:, i);
    filename = strcat('matlab_models/INIT/', ... 
                      strtrim(sample_names(i, :)), '.mat');
    %INIT_model = createTissueSpecificModel(model, options);
    %save(filename,'INIT_model');
end




%% MEM: GIMME

%load(['data' filesep 'expressionRxns_list.mat']);
%load(['data/test' filesep 'test_expressionRxns.mat']);
load(['data/test/>0' filesep 'test_expressionRxns.mat']);
expressionRxns_list_t = expressionRxns_list.';  % Transpose the variable


%% DEBUGGING

options.solver = 'GIMME';
options.threshold = 5*log(2);
options.expressionRxns = expressionRxns_list_t(:, 1);
GIMME_model = createTissueSpecificModel(model, options);
%filename = strcat('data/test/models/', strtrim(sample_names(1, :)), '_GIMME.mat');
filename = strcat('data/test/>0/models/', strtrim(sample_names(1, :)), '_GIMME.mat');
save(filename, 'GIMME_model');



%% Creating and saving the models, one for every sample in the dataset.


for i=1:length(sample_names)
    options.expressionRxns = expressionRxns_list_t(:, i);
    filename = strcat('matlab_models/GIMME', ... 
                      strtrim(sample_names(i, :)), '_options.mat');
    %GIMME_model = createTissueSpecificModel(model, options);
    %save(filename,'GIMME_model');
end


%% MEM: MBA

%load(['data/MBA' filesep 'high_sets']);
%load(['data/test' filesep 'test_high']);
load(['data/test/>0' filesep 'test_high']);
%load(['data/MBA' filesep 'medium_sets']);
%load(['data/test' filesep 'test_medium']);
load(['data/test/>0' filesep 'test_medium']);


%% DEBUGGING

options.solver = 'MBA';
options.high_set = high_sets{1};
options.medium_set = medium_sets{1};
MBA_model = createTissueSpecificModel(model, options);
%filename = strcat('data/test/models/', strtrim(sample_names(1, :)), '_MBA.mat');
filename = strcat('data/test/>0/models/', strtrim(sample_names(1, :)), '_MBA.mat');
save(filename, 'MBA_model');



%% Creating and saving the models, one for every sample in the dataset.

options.solver = 'MBA';

for i=1:length(sample_names)
    options.high_set = high_sets{i};
    options.medium_set = medium_sets{i};
    filename = strcat('matlab_models/MBA/', ...
                      strtrim(sample_names(i, :)), '.mat');           
    %MBA_model = createTissueSpecificModel(model, options);
    %save(filename,'MBA_model');    
end



%% MEM: FASTCORE

%load(['data/FASTCORE' filesep 'core_sets']);
%load(['data/test' filesep 'test_core']);
load(['data/test/>0' filesep 'test_core']);


%% DEBUGGING

options.solver = 'fastCore';
options.core = core_sets{1};
FASTCORE_model = createTissueSpecificModel(model, options);
%filename = strcat('data/test/models/', strtrim(sample_names(1, :)), '_FASTCORE.mat');
filename = strcat('data/test/>0/models/', strtrim(sample_names(1, :)), '_FASTCORE.mat');
save(filename, 'FASTCORE_model');




%% Creating and saving the models, one for every sample in the dataset.

options.solver = 'fastCore';

for i=1:length(sample_names)
    options.core = core_sets{i};
    filename = strcat('matlab_models/FASTCORE/', ... 
                      strtrim(sample_names(i, :)), '.mat'); 
    %FASTCORE_model = createTissueSpecificModel(model, options);
    %save(filename,'FASTCORE_model');    
end



%% MEM: mCADRE

%load(['data/mCADRE' filesep 'ubiquity_sets']);
%load(['data/mCADRE' filesep 'confidence_sets']);

load(['data/test/' filesep 'test_ubiquity']);
load(['data/test/' filesep 'test_confidence']);

%load(['data/test/>0' filesep 'test_ubiquity']);
%load(['data/test/>0' filesep 'test_confidence']);


% Transpose the variables
confidence_sets_t = confidence_sets.';
ubiquity_sets_t = ubiquity_sets.';


%% DEBUGGING
i = 1;
options.solver = 'mCADRE';
options.ubiquityScore = ubiquity_sets_t(:, i);
options.confidenceScores = confidence_sets_t(:, i);
%options.checkFunctionality = 1;
mCADRE_model = createTissueSpecificModel(model, options);
filename = strcat('data/test/models/', strtrim(sample_names(i, :)), '_mCADRE_new.mat');
%filename = strcat('data/test/>0/models/', strtrim(sample_names(i, :)), '_mCADRE.mat');
save(filename, 'mCADRE_model');



%% Creating and saving the models, one for every sample in the dataset.

options.solver = 'mCADRE';

for i=1:length(sample_names)
    options.ubiquityScore = ubiquity_sets_t(:, i);
    options.confidenceScores = confidence_sets_t(:, i);
    filename = strcat('matlab_models/mCADRE/', ... 
                      strtrim(sample_names(i, :)), '.mat');          
    %mCADRE_model = createTissueSpecificModel(model, options);
    %save(filename,'mCADRE_model');
end

