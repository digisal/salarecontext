#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 23 09:43:25 2020

@author: Haavard2
"""

#%% Module imports

import pandas as pd
import numpy as np
import cobra

from collections import defaultdict, Counter
from operator import itemgetter
from itertools import chain


#%% Computing gene scores

def compute_gene_scores(filename, model, quantile=0.90):
    '''
    This function reads the expression data and computes the gene score for 
    each gene in the metabolic model. A gene score is computed for every 
    sample in the expression data.
    
    
    "A gene is defined as active in a sample if its expression value is above 
    a threshold defined for this gene within the dataset considered. 
    The threshold of a gene is defined by the mean value of its expression 
    over all the samples coming from the same dataset with exceptions that 
    the threshold need to be higher or equal the 25th percentile of the 
    overall gene expression value distribution and lower or equal to 
    the 75th percentile." (Richelle et al. 2020) 
    
    However, Opdam et al. (2017) found best prediction accuracies with
    the 90th quantile. Thus, the quantile value used in the computation of
    the gene scores is user-defined.
    
    
    The gene score is computed as follows:  
    
    .. math::

            Gene Score = 5 * log(1 + (\\frac{Expression level}{Threshold})
    

    :param filename:         str, relative path to expression data file
    :param model:            cobra.core.Model
    :param quantile:         float, quantile value used in decimal form
    
    
    :return exprs:           pd.DataFrame, pre-processed the expression data.
    
    :return gene_scores:     pd.DataFrame, the computed gene scores.
    '''
    
    # Read gene expression data as pandas dataframe
    exprs = pd.read_csv(filename, 
                        sep='\t', 
                        header=0)
    
    # Get gene names into a list and set this list as df index
    genes = list(exprs.index)
    
    # Find indices in expression dataset of the genes in the model
    mod_genes_idxs = [
        ind for ind, gene in enumerate(exprs.index) if gene.split(sep=':')[-1] in model.genes
        ]
    
    
    # Subset the data to only contain genes in the model
    exprs = exprs.iloc[mod_genes_idxs, :]
    
    
    # Get gene names (e.g. '106603157') into a list
    genes = [str.split(gene, sep=':')[-1] for gene in exprs.index]
    
    
    # Set gene names list as df index
    exprs = exprs.set_index(keys=pd.Series(genes))
    
    
    # Calculating the 25th percentile to avoid zero division
    q25 = exprs.stack().quantile(q=0.25)

    
    
    # Creating new NumPy matrix for storing gene scores
    score_matrix = np.zeros(exprs.shape)
    
        
    # Iterate over gene expression dataset, beginning with rows, i.e. genes
    for row_idx, (gene, exprs_vals) in enumerate(exprs.iterrows()):
        
        # Assign gene-specific quantile values as threshold to variable
        threshold = exprs_vals.quantile(q=quantile)
        
        if threshold < q25:
            threshold = q25    
        
        
        # Iterate over columns, i.e. samples
        for col_idx, sample in enumerate(list(exprs_vals.index)):
            
            # Assign expression value of that sample to variable
            expr_val = exprs_vals[sample]
    
            # Compute gene score for current gene (row) in current sample (column)
            score = (
                5*np.log(1+(expr_val/threshold))
                )
            
            # Add gene score to score matrix
            score_matrix[row_idx, col_idx] = score
    
    
    # Convert score_matrix to pandas Data Frame
    gene_scores = pd.DataFrame(score_matrix)
    gene_scores.index = exprs.index
    gene_scores.columns = exprs.columns
    
    
    return exprs, gene_scores



#%% Functions from Ove – slightly modified

def preprocess_tasksheet(file):
    """
    Preprocessing of tasksheet into a usable format for further analysis.

    Parameters
    ----------
    file : file
        Excel-file with metabolic task information.

    Returns
    -------
    task_df : Pandas DataFrame
        Containing information about the tasks.
    tasks : List of collections.defaultdicts
        Contains information on metabolite bounds.

    """
    
    # Load metabolic tasks
    task_df = pd.read_excel(file, engine='openpyxl')
    
    # Rename columns
    task_df = task_df.rename(columns={'Unnamed: 12': 'original_tasks'})
    task_df = task_df.drop(columns=['Unnamed: 13'])
    task_df.columns = [c.replace(' ', '_').lower() for c in task_df.columns]
    
    # Prevent forward filling of pathway cells
    task_df.pathways_used = task_df.pathways_used.fillna('')
    
    # Forward fill missing values
    columns = ['id', 'system', 'subsystem', 'description', 'should_fail', 'original_source']
    task_df[columns] = task_df[columns].fillna(method='ffill')
    
    # Fix metabolite IDs
    task_df['in'] = task_df['in'].str.replace('Lcystin', 'cysi_L')
    task_df['out'] = task_df['out'].str.replace('Lcystin', 'cysi_L')
    
    # Reformat system and subsystem names
    task_df.system = task_df.system.str.capitalize().str.replace('&', 'and')
    task_df.subsystem = task_df.subsystem.str.capitalize()
    d = {'&': 'and', 'tp ': 'TP ', 'mp ': 'MP ', ' nad ': ' NAD ', ' fad ': ' FAD ', 'b6': 'B6', ' a ': ' A ', 'Gpi ': 'GPI ', ' ros ': ' ROS ', ' / ': '/'}
    for k, v in d.items():
        task_df.subsystem = task_df.subsystem.str.replace(k, v)
    
    # Change column types
    task_df.id = task_df.id.astype(int)
    task_df.should_fail = task_df.should_fail.astype(bool)
    
    # Change compartments that are not in model to cytoplasm
    for col in ('in', 'out'):
        for comp in ('r'):
            task_df[col] = task_df[col].str.replace('\[' + comp + '\]', '[c]')
            
    
    # Ensure that amino acid synthesis tasks reflect essentiality in salmon
    aa_ess = {
    'Alanine': False,
    'Arginine': True,
    'Asparagine': False,
    'Aspartate': False,
    'Cysteine': False,
    'Glutamine': False,
    'Glutamate': False,
    'Glycine': False,
    'Histidine': True,
    'Isoleucine': True,
    'Leucine': True,
    'Lysine': True,
    'Methionine': True,
    'Phenylalanine': True,
    'Proline': False,
    'Serine': False,
    'Threonine': True,
    'Tryptophan': True,
    'Valine': True
    }

    for aa, ess in aa_ess.items():
        task_df.loc[task_df.description.str.startswith(aa + ' synthesis'), 'should_fail'] = ess
        
    # Cysteine synthesis should fail without serine and methionine
    task_df.loc[task_df.id == 82, 'should_fail'] = True
    
    # Synthesis of asparagine from aspartate and glutamine should fail
    task_df.loc[task_df.id == 67, 'should_fail'] = True
    task_df.loc[task_df.id == 75, 'should_fail'] = True
    
    
    # Get list of tasks
    columns = ['in', 'in_lb', 'in_ub', 'out', 'out_lb', 'out_ub']
    
    tasks = []
    
    for i, df in task_df.groupby('id'):
        df = df[columns]
        df = df.fillna(value={'in_lb': 0, 'out_lb': 0, 'in_ub': 1000, 'out_ub': 1000})
        
        # Get metabolites and their bounds
        task = defaultdict(dict)
        #mapped = True
        for x in ('in', 'out'):
            for j, row in df[[c for c in df.columns if c.startswith(x)]].dropna().iterrows():
                m = row[x][:-1].replace('[', '_').replace('_D', '__D').replace('_L', '__L').replace('_R', '__R')
                lb, ub = row[[x + '_lb', x + '_ub']]
                task[x][m] = lb, ub
        
        tasks.append(task)
    
    # Correct coefficient of CO2 in methionine degradation
    tasks[115]['out']['co2_c'] = (2.0, 5.0)
    
    # Drop task columns and resulting duplicate rows from data frame
    task_df = task_df.drop(
        columns=columns
        ).drop_duplicates(subset=['id'], ignore_index=True).dropna(how='all')
    
    return task_df, tasks




def create_task_reactions(model, task):
    """Creates reactions to represent a metabolic task in a model."""
    
    reactions = []
    
    for x, d in task.items():
        c = 1 if x == 'in' else -1
        for m_id, bounds in d.items():     
            try:
                m = model.metabolites.get_by_id(m_id)
            except KeyError:
                return []
            
            r = cobra.Reaction(m.id + '_' + x)
            r.add_metabolites({m: c})
            r.bounds = bounds
            reactions.append(r)
            
    return reactions




def run_task(model, task, method='fba', solution='object'):
    """Checks if model can perform a metabolic task."""
      
    if solution not in ['object', 'bool']:
        raise KeyError("Sol must be either 'object' or 'bool'.")
    if method not in ['fba', 'pfba']:
        raise KeyError("Method must be either 'fba' or 'pfba'.")
    
    # Create task reactions
    reactions = create_task_reactions(model, task)
    if not reactions:
        if solution == 'object':
            return 'Infeasible'
        elif solution == 'bool':
            return False


    # Get bounds of boundary reactions before setting them to zero
    bounds = {r: r.bounds for r in model.boundary}
    for r in model.boundary:
        r.bounds = 0, 0
        
    # Add task, solve model, and remove task
    model.add_reactions(reactions)
    
    if method == 'fba':
        sol = model.optimize()
        #status = (sol.status == 'optimal')
    elif method == 'pfba':
        try:
            sol = cobra.flux_analysis.pfba(model, fraction_of_optimum=0.9)
            #status = (sol.status == 'optimal')
        except:
            if solution == 'object':
                sol = 'Infeasible'
            elif solution == 'bool':
                sol = False
            #status = False


    model.remove_reactions(reactions)
    
    # Reset bounds of boundary reactions
    for r in model.boundary:
        r.bounds = bounds[r]
        
    if solution == 'object':
        return sol
    elif solution == 'bool':
        return sol.status == 'optimal'





def create_gpr_chart(model, task_path, method='pfba'):
    """
    Creates a score chart of passed tasks. Columns include description of 
    metabolic task, FBA/pFBA-solution object, as well as required reactions
    and associated genes required to perform the tasks.

    Parameters
    ----------
    model : cobra.core.Model
        The metabolic model of interest.
    task_path : str
        Path to tasksheet.
    method : str, optional
        FBA method of choice (one of 'fba' or 'pfba'). The default is 'pfba'.

    Returns
    -------
    task_sc : pd.DataFrame
        DataFrame containing task description, solution object of 
        (p)fba-analysis, as well as required reactions and associated 
        genes extracted from the solution objects.

    """

    # Load and prepare tasksheet
    task_df, tasks = preprocess_tasksheet(task_path)
    
    # Run all metabolic tasks and add results to data frame
    task_sc = task_df.copy()
    task_sc['solution'] = [run_task(model, task, method = method, 
                                    solution = 'object') 
                           for task in tasks]
    
    # Create DataFrame with only relevant columns and rows and reset index
    task_sc = task_sc.loc[task_sc.solution != 'Infeasible', 
                          ('description','solution')]
    task_sc = task_sc.reset_index(drop=True)
    
    
    # Add new columns for storing associated reactions and genes
    task_sc['reactions'] =  np.empty((len(task_sc), 0)).tolist()
    task_sc['genes'] = np.empty((len(task_sc), 0)).tolist()
    
    
    # Iterate over solutions in DataFrame
    for ind, sol in enumerate(task_sc['solution']):
        
        # Create dictionaries for storing component information
        reactions = {}
        genes = []
        
        # Iterate over solution information
        for rx, flux in sol.fluxes.items():
            if abs(flux) > 1e-06:              # Change threshold to e.g. 1e-6
                reactions[rx] = flux           # Maybe e.g. round(flux, 4)
                try:
                    gns = model.reactions.get_by_id(rx).genes
                    if len(gns):
                        genes += [x for x in gns]
                except KeyError:
                    continue
        
        # Add the set of reactions and associated genes required to pass a task
        task_sc.reactions[ind] = reactions    
        task_sc.genes[ind] = list(set(genes)) # Removing duplicates from gene list
    

    return task_sc


#%% Making MT-score dataframes


def compute_task_scores(model, task_sc, gene_scores, binary=True):
    '''
    This function computes a score for each metabolic task (MT) for each
    sample in the provided expression data.
    
    
    "Each reaction involved in a task is associated with a reaction activity 
    level (RAL) that corresponds to the preprocessed gene expression value 
    of the gene selected as the main determinant for this reaction. We also 
    computed the significance of each gene selected with regard to its overall 
    use in the observed condition. Actually, some genes will be mapped to 
    multiple reactions (e.g. promiscuous enzyme). Therefore, we assume that 
    there may exist some competition between the reactions using this gene. 
    We define the significance of a gene (S) by its specificity for a reaction
    by the inverse of the number reactions in which this gene is used as the
    main determinant. Finally, the metabolic score can be computed as the mean
    of the product of the activity level of each reaction with the 
    significance of its associated gene:" (Richelle et al. 2020) 
    
    
    The MT score is computed as follows:  
    
    .. math::

            MT Score = \\frac{SUM{RAL*S}}{
                number of reactions involved in the task
                }
    

    :param model:           cobra.core.Model
    
    :param task_sc:         pd.DataFrame, Score chart containing task 
                            description, solution object of 
                            (p)fba-analysis, as well as required reactions 
                            and associated genes extracted from the 
                            solution objects.
    
    :param gene_scores:     pd.DataFrame, the computed gene scores.
    
    :param binary:          bool, Specifies whether binary version of matrix
                            should be returned. The binary values determine 
                            whether a metabolic task is active or not 
                            based on a gene expression profile (gene scores).
    
    
    :return MT_df:          pd.DataFrame, the MT scores for each sample.
    '''
    
    
    # Create empty Pandas DataFrame to store the results
    MT_df = pd.DataFrame(index = task_sc.description, 
                         columns = gene_scores.columns)
    
    
    # Iterate over every sample and compute the MT scores for each task
    for sample, scores in gene_scores.iteritems():
        
        # Creating empty MT_scores dict, intended for Task description as keys 
        # and metabolic task score as values
        MT_scores = {}
        
        
        # Iterate over tasks
        for ind, task in enumerate(task_sc.reactions):
            RAL_scores = {}  # Dict for RAL scores
            
            # Iterate over reactions within each task
            for rx in task.keys():
                scrs = {}  # Dict for gene scores
                
                # Iterate over genes within each reaction
                try:
                    for g in model.reactions.get_by_id(rx).genes:
                        gene = str(g) # convert to string to facilitate indexing
                        
                        if gene in scores:
                            scr_val = scores[gene]
                            if isinstance(scr_val, pd.Series):
                                scr_val = max(scr_val)
                                
                            scrs[gene] = scr_val # Add {gene: value}
                            
                except KeyError:
                    continue
                    
                # Adding reaction, main determinant gene and expression value to RAL_scores dict
                if len(scrs):
                    max_gene, max_value = max(scrs.items(), key = itemgetter(1))
                    RAL_scores[rx] = {max_gene: max_value}
            
            if binary:
                
                # Add all RALs to list
                RAL_lst = [ral for rx in RAL_scores.values() for ral in rx.values()]
                
                # Compute RAL mean and test activity with threshold
                if len(RAL_lst):
                    RAL_mean = np.mean(RAL_lst)
                else:
                    RAL_mean = 0
                
                # Add binary classifier to relevant dictionary
                task_desc = task_sc.description[ind]
                
                # Active task in current sample
                if RAL_mean > 5*np.log(2):
                    MT_scores[task_desc] = 1
                
                # Inactive task in current sample
                else:
                    MT_scores[task_desc] = 0
    
            else:
                # Get frequencies of main determinant genes in for this task
                lst = [list(k.keys()) for k in RAL_scores.values()]
                main_genes = list(chain.from_iterable(lst))
                gene_frequencies = Counter(main_genes)
                
                # Iterate over each reaction and sum RAL with gene significance (S)
                product_list = []
                for rx in RAL_scores.values():
                    for gene, value in rx.items():
                        product_list.append(value*(gene_frequencies[gene]**-1))
                
                # Compute MT score and add to relevant dictionary
                task_desc = task_sc.description[ind]
                
                if len(product_list):
                    MT_scores[task_desc] = sum(product_list)/len(product_list)
                else:
                    MT_scores[task_desc] = 0
                    
            
        MT_df[sample] = MT_df.index.map(MT_scores) 
        
    return MT_df



#%%




# if __name__ == "__main__":
#     import doctest
#     doctest.testmod()
