# Extracting functionally accurate context-specific models of Atlantic salmon metabolism

Code that reproduces the results from Molversmyr et al. [1] is found in [figures_paper.ipynb](data_analysis/figures_paper.ipynb). All necessary data is provided here and loaded in the Jupyter notebook.

[1] Molversmyr, H., Øyås, O., Rotnes, F., & Vik, J. O. Extracting functionally accurate context-specific models of Atlantic salmon metabolism. *Research Square* (2023) https://doi.org/10.21203/rs.3.rs-2492302/v1